// es como un esquema para saber los datos que se traen de la bbdd.
const mongoose = require("mongoose"); 
//Dentro de mongoose
const Schema = mongoose.Schema;

//Aqui definimos nuestro esquema.
const homeSchema = new Schema({
  id: {
    type: Number,
    required: true,
  },
  homeName: {
    type: String,
    required: true,
  },
  homeTitle: {
    type: String,
    required: true,
  },
  homeImage: {
    type: String,
    required: true,
  },
  homeDescription: {
    type: String,
    required: true,
  },
});

const home = mongoose.model("home", homeSchema);

module.exports = home;
