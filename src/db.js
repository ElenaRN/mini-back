
//Este archivo crea la conexion con la base de datos. En este caso es una bbdd con mongo.

const mongoose = require("mongoose");
//aqui esta la URL de nuestra base de datos. en este caso como no tenemos ninguna repositada ponemos eso.
const URI = "mongodb://localhost:27017/blog-crud";

mongoose.connect(URI, {
//esto copiar y pegar no cambia.
useNewUrlParser: true,
useUnifiedTopology: true,
})
.then((db) => console.log("Db is conected"))  //then para conectarlo a la base de datos
.catch((err) => console.log(err));

module.exports = mongoose;
