//me voy a ese fichero antes de seguir.
const Home = require("../models/home.model");

const homeController = {};

//Se dan unos atributos, que son los elementos que van  a definir o atacar,
// para recuperar datos, pushearlos, postearlos, etc.
//recibe 2 parametros(request=>lo que entra del navegador,
//response=> lo que sale de nuestro servidor como respuesta al navegador.)
homeController.getHome = (req, res) => {
  console.log("holaaa");
  //Hacemos una promesa para tener todos los datos de home y le damos el valor
  return Home.find()
    .sort({ id: "ascendente" })
    .then((home) => {
      const result = { result: home };
      //Sera un 200 si pasa por aqui y asi devuelve el json con el elemento. si no entra en el catch
      return res.status(200).json(result);
    })
    .catch((err) => {
      return res.status(500).send(err.message);
    });
};

module.exports = homeController;
