const express = require("express");

//necesitamos aqui la parte del router para definir el Endpoint.
const router = express.Router();
//vamos a ese fichero.
const homeController = require("../controllers/home.controller"); //primer endpoint.

router.get("/", homeController.getHome);

module.exports = router;
