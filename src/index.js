const express = require("express");
//const morgan = require("morgan");

const server = express(); // con esta variable ejecutamos la libreria express.

require("./db");

//vamos a configurar como va a ser nuestro proyecto y levantarlo-

const PORT = 3001; // puerto que tenemos en local.

//Significa que si hay algun puerto en nuestras varibles d entorno,
//cogeme el puerto de variables de entorno (process.env.PORT), y si no hay, cogeme el puerto que yo te diga (PORT)
server.set("port", process.env.PORT || PORT);

//*process data -> hace el efecto como un middleware
//Utilizamos morgan , para ver que peticion estoy haciendo.
//server.use(morgan("dev"));
//Todo lo relacionado con express me lo ejecutará como un json.
//server.use(express.json());

//*Create Endpoints -> es el fichero de routes. y vamos a ese fichero.
//server.use("/api/home", require("./routes/home.routes"));

server.use('/', (req, res) => {​​
  res.send('Hello Home!');
  }​​);
//Resolve CORS, para resolver errores en cabeceras.
//Es para que cuando nos conectemos con nuestra app, que no de problemas a la hora de recuperar la información.
/* server.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
    "Access-Control-Allow-Headers",
    "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method"
    );
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
    res.header("Allow", "GET, POST, OPTIONS, PUT, DELETE");
    next();
    }); */


//*Init server -> Create  or start server
//Escucha el puerto,
server.listen(server.get("PORT"), () => {
  console.log(`server created in http://localhost:${PORT}`);
}); // ejecutamos (npm run dev) para ver si escucha a nuestro server.
